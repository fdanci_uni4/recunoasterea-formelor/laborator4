package ro.usv.rf;

public class USVInputFileCustomException extends Exception {
	public USVInputFileCustomException(String text) {
		super(text);
	}
}
