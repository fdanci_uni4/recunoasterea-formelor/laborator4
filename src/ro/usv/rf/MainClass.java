package ro.usv.rf;

import java.util.ArrayList;
import java.util.List;

public class MainClass {

	static double round(double number, int numDigits) {
		return Math.floor(number * 100) / 100;
	}
	

	public static void main(String[] args) {
		double[][] learningSet;

		try {

			learningSet = FileUtils.readLearningSetFromFile("in.txt");
			int numberOfPatterns = learningSet.length;
			int numberOfFeatures = learningSet[0].length-1;
			System.out.println(String.format("The learning set has %s patterns and "
					+ "%s features", numberOfPatterns, numberOfFeatures));

			List<double []> patternList = new ArrayList<double[]>();
			double [] classes = new double[numberOfPatterns];
			for (int formIndex = 0; formIndex < numberOfPatterns; formIndex++)
			{
				double [] xCoordinate = new double[numberOfFeatures];
				for(int featureIndex=0; featureIndex<numberOfFeatures; featureIndex++ )
				{
					xCoordinate[featureIndex]=learningSet[formIndex][featureIndex];
				}
				classes[formIndex]=learningSet[formIndex][numberOfFeatures];
				patternList.add(xCoordinate);
			}
			//Matricea distantelor
			double [][]mat=new double[numberOfPatterns][numberOfPatterns];
			for (int patternIndex = 0; patternIndex <mat.length ; patternIndex++)
			{
				for(int featureIndex=0; featureIndex<patternIndex; featureIndex++ )
				{
					mat[patternIndex][featureIndex] = round(DistanceUtils.EuclidianDistances(patternList
							.get(patternIndex), patternList.get(featureIndex)), 2);
					mat[featureIndex][patternIndex] = mat[patternIndex][featureIndex];
				}
			}

			System.out.println();
			for(int i=0;i<mat.length;i++)
			{
				for(int k=0;k<mat[0].length;k++) {
					System.out.printf("%.2f  ", mat[i][k]);
				}
				System.out.println();
			}
			System.out.println();

			int index=0;
			for(int indexClass=0;indexClass<classes.length;indexClass++)
			{
				if(classes[indexClass]==0.0)
				{
					System.out.println();	
					index=DistanceUtils.NN(mat, indexClass);
					learningSet[indexClass][numberOfFeatures]=learningSet[index][numberOfFeatures];
					System.out.println(learningSet[indexClass][numberOfFeatures]);
				}
			}


		} catch (USVInputFileCustomException e) {
			System.out.println(e.getMessage());
		} finally {
			System.out.println("Finished learning set operations");
		}
	}

}
