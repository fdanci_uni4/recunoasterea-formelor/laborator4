package ro.usv.rf;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;

public class DistanceUtils {
	public static double EuclidianDistances(double[] pattern1, double[] pattern2) {
		List<Double> numbers = new ArrayList<>();
		for (int i = 0; i < pattern1.length; i++) {
			numbers.add(Math.pow(pattern1[i] - pattern2[i], 2));
		}
		
		return Math.sqrt(numbers.stream().mapToDouble(i -> i).sum());
	}

//	public static double NN(double[] ds) {
//		double min = DoubleStream.of(ds).min().getAsDouble();
//		return min;
//	}

	public static double NN(double[][] mat, int indexClass) {
		// TODO Auto-generated method stub
		return mat[4][indexClass];
	}
}
